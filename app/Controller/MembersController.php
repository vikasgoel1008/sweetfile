<?php
App::uses('apiLivedrive', 'Vendor');
App::uses('Stripe', 'Vendor');
App::uses('CakeEmail', 'Network/Email');
	
class MembersController extends AppController {
	public $components = array('Stripe','Auth'=>array(
										'loginAction'=>array(
												'controller'=>'Members',
												'action'=>'login'
										),
										'authenticate'=>array(
											'Form'=>array(
												'fields'=>array('username'=>'email'),
												'userModel'=>'Member'
											)
										)
							));
	function beforeFilter() {
		$this->Auth->allow('process','ldtest');
	}
	function ldtest($member_id,$email,$pass,$name){
		$chars ="1234567890";
	    $sub_domain='';
	    for($i=0;$i<6; $i++)
	    {
	        $sub_domain .= $chars[ rand(0,strlen($chars)-1)];
	    }
	    $sub_domain = $sub_domain;
		$explode_name = explode(' ',$name);
		$first_name = $explode_name['0'];
		if(isset($explode_name['1']))
		{
			$last_name = $explode_name['1'];
		}
		else
		{
			$last_name = $explode_name['0'];
		}
		$liveDrive = new apiLivedrive;
		$liveDrive->addUser($email, $pass, $sub_domain, 'Unlimited', 'Unlimited', false, true, $first_name, $last_name, '123', 'Backup');
		if($liveDrive->success)
		{
			$data['id'] = $member_id;
			$data['livedrive_id'] = $liveDrive->newUserId;
			$this->Member->save($data);
			//echo "Success! New member id is " . $liveDrive->newUserId;
			$liveDrive->getUser($liveDrive->newUserId);
			//echo "<br/></br><br/>";
			if($liveDrive->success){
				//echo "Success! username is " . $liveDrive->userName;
			} else {
				//echo "Failed because of " . $liveDrive->codeReason;
			}
			//echo "<br/></br><br/>";
			$liveDrive->updateUser($liveDrive->newUserId, $email, $pass, $sub_domain, false, true, $first_name, $last_name);
			if($liveDrive->success){
				//echo "Success! User was updated";
			} else {
				//echo "Failed because of " . $liveDrive->codeReason;
			}
			//echo "<br/></br><br/>";
			//Send Email to user
			$value['Email'] = $email;
			$value['Name'] = $first_name;
			$value['Password'] = $pass;
			
			$emailConfig = new CakeEmail();
			$emailConfig->config('smtp');
			$emailConfig->emailFormat('html');
			$emailConfig->to($email);
			$emailConfig->subject('Welcome to Sweet File');
			$emailConfig->viewVars(array('value' => $value));
			$emailConfig->template('NewUser');
			$emailConfig->helpers('Html');
			$emailConfig->send();
		
			$this->set('email',$email);
			$this->set('pass',$pass);
			$this->layout = 'user';
			$this->render('welcome');
		} else {
			echo "Failed because of " . $liveDrive->codeReason;
			$this->autoRender = false;
		}
	}
	
	
	function process(){
		$this->autoRender = false;
		if ($this->request->is('post')) {
			//print_r($_POST);die;
			$db_amount = $_POST['plan_price']/100;
			$this->loadModel('Plan');
			$plan_id = $this->Plan->getPlanId($_POST['plan_name'],$db_amount);
			if(!empty($plan_id))
			{
				Stripe::setApiKey("sk_test_tR6DNVieMrK26yqa2xub0oKI");
				if($this->Auth->loggedIn()){
					
				}
				else
				{
					$token = $_POST['stripeToken'];
					$charge_customer = Stripe_Customer::create(array( 
						"description" => "Customer for sweetfile.com", 
						"card" => "$token",
						"email" => $_POST['stripeEmail']
						
					));
					$cust_details = json_decode($charge_customer);
					$cust_id = $cust_details->id;
					
					$customer = Stripe_Customer::retrieve($cust_id); 
					$customer->subscriptions->create(array("plan" => $plan_id));
					
				}
				$charge = Stripe_Charge::create(array(
				  "amount" => $_POST['plan_price'], // amount in cents, again
				  "currency" => "usd",
				  "customer" => $cust_id,
				  "description" => "payinguser@example.com")
				);
				$all_details = json_decode($charge);
				
				//Save stripe ID here
				if(($all_details->paid==1))
				{
					$status = "active";
					$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
					$password = '';
					for ($i = 0; $i < 10; $i++) {
						$password .= $characters[rand(0, strlen($characters) - 1)];
					}
					// Create customer record in member's table
					$this->Member->create();
					$this->Member->set(array(    
						'email' => $_POST['stripeEmail'],
						'name' => $all_details->card->name,  
						'address1' => $all_details->card->address_line1,  
						'postal' => $all_details->card->address_zip,  
						'city' => $all_details->card->address_city,  
						'state' => $all_details->card->address_state,  
						'country' => $all_details->card->address_country, 
						'ip' => $_SERVER['REMOTE_ADDR'],
						'date_reg' => date("Y-m-d H:i:s",mktime()),
						'stripe_id' => $cust_id,
						'status'=> $status,
						'password'=>$password,
						'plan_id'=>$plan_id
						));
					if($this->Member->save())
					{
						$member_id = $this->Member->getLastInsertId();
						//create a record in payments table
						$payment_data['member_id'] = $member_id;
						$payment_data['stripe_id'] = $cust_id;
						$payment_data['card_id'] = $all_details->card->id;
						$payment_data['charge_id'] = $all_details->id;
						$payment_data['last4'] = $all_details->card->last4;
						$payment_data['amount'] = $db_amount;
						$payment_data['currency'] = $all_details->currency;
						$payment_data['date'] = date('Y-m-d H:i:s');
						$payment_data['status'] = 1;
						$this->loadModel('Payment');
						$this->Payment->create();
						$this->Payment->save($payment_data);
						$this->ldtest($member_id,$_POST['stripeEmail'],$password,$all_details->card->name);
					}
				}
			}
		}
	}
	public function login()
	{
		$this->layout = "user";
		if($this->request->is('post'))
		{
			if ($this->Auth->login())
			{
				//$this->redirect($this->Auth->redirect());
				$this->redirect(array('controller'=>'Members','action'=>'dashboard'));
			}
			else
			{
				$this->set('error',1);
			}
		}
	}
	public function logout()
	{
		$this->redirect($this->Auth->logout());
	}
	public function dashboard()
	{
		$this->layout = "user";
	}
	public function downloads()
	{
		$this->layout = "user";
	}
	public function profile()
	{
		$this->layout = "user";
		$userID = $this->Session->read('Auth.User.id');
		$userData = $this->Member->findById($userID);
		
		if(!empty($this->request->data))
		{
			if($userID==$this->request->data['Member']['id'])
			{
				if(isset($this->request->data['Member']['oldpass']))
				{
					$oldpass = AuthComponent::password($this->request->data['Member']['oldpass']);
					if(($oldpass==$userData['Member']['password'])&&($this->request->data['Member']['newpass']==$this->request->data['Member']['confpass']))
					{
						$this->request->data['Member']['password'] = $this->request->data['Member']['newpass'];
					}
					else
					{
						$this->set('error','Password Mismatch error. Please try again');
					}
				}
				if($this->Member->save($this->request->data))
				{
					if(isset($this->request->data['Member']['oldpass']))
					{
						$this->set('success','Password Updated Successfully');
					}
					else
					{
						$this->set('success','Profile Updated Successfully');
					}
				}
			}
		}
		$this->data = $userData;	
	}
}