
	<body>
    	<!--Start Preloader-->
        <div id="preloader">
            <div class="preloader-container">
                <h4 class="preload-logo wow fadeInLeft">Sweet File</h4>
                <h4 class="back-logo wow fadeInRight">Sweet File</h4>
                <img src="img/preload.gif" class="preload-gif wow fadeInUp">
            </div>
        </div>
        <!--End Preloader-->
		<!--Start Home-->
    	<section id="home" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">
        	<div class="parallax-overlay"></div> 
            <div class="home-container text-center">
            	<div class="home-title liquid-slider" id="slider-home" style="z-index:3;">
                	<div>
                        <h1><small class="white">Sweet File</small> <br><strong>Worry-Free Backup</strong></h1>
                        <p class="lead">Automatic backups of all your important files and documents.</p>
                        <div class="home-btn">
                            <h4 class="btn-home"><a href="#services">LEARN MORE</a></h4>
                        </div>
                    </div>
                    <div>
                        <h1><small class="white">Sweet File</small> <br><strong>Disaster Proof</strong></h1>
                        <p class="lead">Losing your pictures due to a hard drive crash is a thing of the past.</p>
                        <div class="home-btn">
                            <h4 class="btn-home"><a href="#services">HOW WE DO IT</a></h4>
                        </div>
                    </div>
                    
            	</div>
            </div>
            <div class="home-bottom">
            	<div class="container text-center">
                    <div class="move">
                        <a href="#services" class="fa fa-chevron-down"></a>
                        <div class="dots"></div>  
                    </div>  
            	</div> 
            </div>
        </section>
        <!--End Home-->
        <!--Start Header-->
        <section id="fixed-navbar">
			<nav class="navbar navbar-default navbar-static-top" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					    </button>
                        <!--<a class="navbar-brand" rel="home" href="/" title="Sweet File"><h3>Sweet File</h3></a>-->
                        <a class="navbar-brand" rel="home" href="/" title="Sweet File"><img src='img/sweetfile.gif' height="50"></a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="main-nav">
                      <ul class="nav navbar-nav  navbar-right">
                        <li><a href="#home">Home</a></li>
                        <li><a href="#services">About</a></li>
                        <li><a href="#price-list"><span class="label label-primary">Pricing</span></a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#contact-form">Contact</a></li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
				</div><!-- /.container -->
			</nav>
		</section>
        <!--End Header-->
        <div class="site-wrapper">

            <!-- Start Services -->
            <section id="services">
                <div class="container">
                    <div class="col-lg-12 section-title wow flipInX">
                        <h2><small>Automated Backup.... Perfected</small><br>Back up all your files and access them anywhere.</h2>
                        <p class="lead">Our automated backup software gives you <span class="highlight">peace of mind</span>. Never again worry about manually copying files to a second or third location for safekeeping. We do it for you, instantly and constantly. So you can get on with your day.</p>
                    </div>
                    <div class="services-container">
                        <div class="row services-row">
                            <div class="col-md-4 col-sm-6 wow flipInX">
                                <div class="service">
                                    <div class="service-icon">
                                        <i class="icon ion-looping ion-3x highlight"></i>
										<i class="icon ion-loop back-icon"></i>
                                    </div>
                                    <div class="service-info">
                                      <h4>Always On<br><small>Automatic Backup</small></h4>
                                        <p class="service-description">Our software monitors for changes in your files and automatically saves them to the cloud as needed.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 wow flipInY">
                                <div class="service">
                                    <div class="service-icon">
                                    <i class="icon ion-upload ion-3x highlight"></i>
                                    <i class="icon ion-upload back-icon"></i>
                                    </div>
                                    <div class="service-info">
                                      <h4>Cloud Storage<br><small>Infinite Possibilities</small></h4>
                                        <p class="service-description">Storing your important documents securely in the cloud makes them always available.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 wow flipInX">
                                <div class="service">
                                    <div class="service-icon">
                                    <i class="icon ion-ios7-infinite ion-3x highlight"></i>
                                    <i class="icon ion-ios7-infinite back-icon"></i>
                                    </div>
                                    <div class="service-info">
                                      <h4>Unlimited Backups<br><small>Backup Everything You Have</small></h4>
                                        <p class="service-description">Unlike other online backup solutions, Sweet File has no limits whatsoever. Backup as much or as little as you want.</p>
                                    </div>
                                </div>
                            </div>                
                            <div class="col-md-4 col-sm-6 wow flipInY">
                                <div class="service">
                                    <div class="service-icon">
                                    <i class="icon ion-iphone ion-3x highlight"></i>
                                    <i class="icon ion-iphone back-icon"></i>
                                    </div>
                                    <div class="service-info">
                                      <h4>Mobile Ready<br><small>iOS & Android Access</small></h4>
                                        <p class="service-description">All of your files are always at your fingertips. Access your files remotely and backup your mobile phone data on iOS and Android.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 wow flipInX">
                                <div class="service">
                                    <div class="service-icon">
                                    	<i class="icon ion-help-buoy ion-3x highlight"></i>
                                        <i class="icon ion-help-buoy back-icon"></i>
                                    </div>
                                    <div class="service-info">
                                      <h4>World Class<br><small>Customer Support</small></h4>
                                        <p class="service-description">The best service and support in the industry, hands down.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 wow flipInY">
                                <div class="service">
                                    <div class="service-icon">
                                        <i class="icon ion-ios7-locked ion-3x highlight"></i>
                                        <i class="icon ion-ios7-locked back-icon"></i>
                                    </div>
                                    <div class="service-info">
                                      <h4>Secure Backup<br><small>No One Else Can See Your Data</small></h4>
                                        <p class="service-description">We encrypt all of your files before storing them, keeping them 100% safe.</p>
                                    </div>
                                </div>
                            </div>                
                        </div>
                	</div>
                </div>
            </section>
            <!-- End Services -->
            
            <!-- Start Separator Call to Action -->
            <section id="separator-purchase" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">
            	<div class="container">
                    <div class="col-sm-12 call-action-purchase text-center">
                        <div class="action-5-info">
							<h3>What will happen if your hard disk dies today? What happens to the 1000's of pictures of your family?<br/><small> With Sweet File your important pictures, documents, and data are always safe and secure. No worries.</small></h3>
                        </div>
						<div class="action-5-btn">
							<a href="#price-list" class="btn btn-primary btn-lg btn-responsive">See Our Plans and Prices</a>
						</div>
					</div>
                </div>
            </section>
            <!-- End Separator Purchase -->
            
            <!-- Start Separator Testimonials -->
            <section id="separator-testimonials" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">
                <div class="row text-center" style="position:relative;">
                	<div class="parallax-overlay"></div> 
                    <div class="liquid-slider" style="z-index: 3;" id="testimonials-slider">
                    	<div class="col-lg-12 testimonials text-center">
                        	<img class="testimonial-img" src="img/testimonial-1.jpg"/>
                        	<h4><i class="fa fa-quote-left highlight"></i> <small class="white">Sweet File is great! I lost all of my music, pictures, and school files a few months back.</small> <span class="highlight">Since I started using Sweet File I don't worry about that anymore.</span> <i class="fa fa-quote-right highlight"></i></h4>
                        	<p class="label label-primary">Niko Gray - NYC</p>         
                        </div>
                        <div class="col-lg-12 testimonials text-center">
                        	<img class="testimonial-img" src="img/testimonial-2.jpg"/>
                        	<h4><i class="fa fa-quote-left highlight"></i> <small class="white">I have been using Sweet File for the past 3 months while it was in beta testing. </small> <span class="highlight">It's impressive software that does exactly what it says</span>, and it does it well.<i class="fa fa-quote-right highlight"></i></h4>
                        	<p class="label label-primary">Jennifer L. - Jacksonville, FL</p>         
                        </div>
                        <div class="col-lg-12 testimonials text-center">
                        	<img class="testimonial-img" src="img/testimonial-3.jpg"/>
                        	<h4><i class="fa fa-quote-left highlight"></i> <small class="white">Sweet File is simple to use and easy to set up.</small> <span class="highlight">What more could you want?</span> <i class="fa fa-quote-right highlight"></i></h4>
                        	<p class="label label-primary">Gary Coats - Spartanburg, SC</p>         
                        </div>
                	</div>
                </div>
            </section>
        	<!-- End Separator Testimonials -->
            
            <!-- Start Price List -->
            <section id="price-list">
                <div class="container">
                    <div class="col-lg-12 section-title-price wow flipInX">
                        <h2><small>Our Pricing Plans.</small><br><strong>We have a plan for every budget.</strong></h2>
                        <p class="lead">Pick a plan that <span class="highlight">best suits</span> your needs.</p>
                    </div>
                    <div class="price-container">     
                    	<div class="row">   
                        	
							<div class="col-md-4 col-sm-4 wow flipInY">
                            	<div class="price-box text-center">
                               		<div class="price-box-info">
                                    	<h1>Bronze</h1>
                                        <h2><span>$</span>7<small>/month</small></h2>
                                    </div>  
                                    <div class="price-box-offer">
                                       <ul class="offer-list">
                                       	<li><span>Full</span> Email Support</li>
                                        <li>Unlimited Backups</li>
                                        <li>All File Types Accepted</li>
                                        <li>Keep <span>30 Versions</span> of Any File</li>
                                        <li>Access Your Files From Your Mobile</li>
                                        <li>Access Your Files From Your Browser</li>
                                        <li>Restore Deleted Files For <span>30 Days</span></li>
                                       </ul>
                                    </div>
                                    <div class="sign-up-btn">
                                    	<form action="members/process" method="POST">
										  <script
										    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
										    data-key="pk_test_TKe0SLjhgAXUpAOJxnsVTko5"
										    data-amount="700"
										    data-name="Sweet File"
										    data-description="Bronze"
										    data-billing-address="true"
										    data-allow-remember-me="false"
										    data-image="img/sweetfilesquare.jpg">
										  </script>
										  <input type="hidden" name="plan_name" value="Bronze"></input>
										  <input type="hidden" name="plan_price" value="700"></input>
										</form>
                                    </div>
                            	</div>           
                            </div>
                            
                            <div class="col-md-4 col-sm-4 wow flipInX">
                            	<div class="price-box-big text-center">
                               		<div class="price-box-info">
                                    	<h1>Gold</h1>
                                        <h2><span>$</span>30<small>/year</small></h2>
                                    </div>  
                                    <div class="price-box-offer">
                                       <ul class="offer-list">
                                       	<li><span>Full</span> Email Support</li>
                                        <li>Unlimited Backups</li>
                                        <li>All File Types Accepted</li>
                                        <li>Keep <span>30 Versions</span> of Any File</li>
                                        <li>Access Your Files From Your Mobile</li>
                                        <li>Access Your Files From Your Browser</li>
                                        <li>Restore Deleted Files For <span>30 Days</span></li>
                                       </ul>
                                    </div>
                                    <div class="sign-up-btn">
                                    	<form action="members/process" method="POST">
										  <script
										    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
										    data-key="pk_test_hr3S0Qu6OFXSP6MkQRyAMpnV"
										    data-amount="3000"
										    data-name="Sweet File"
										    data-description="Gold"
										    data-billing-address="true"
										    data-allow-remember-me="false"
										    data-image="img/sweetfilesquare.jpg">
										  </script>
										  <input type="hidden" name="plan_name" value="Gold"></input>
										  <input type="hidden" name="plan_price" value="3000"></input>
										</form>
                                    </div>
                            	</div>           
                            </div>
                            
                            <div class="col-md-4 col-sm-4 wow flipInY">
                            	<div class="price-box text-center">
                               		<div class="price-box-info">
                                    	<h1>Silver</h1>
                                        <h2><span>$</span>15<small>/qtr</small></h2>
                                    </div>  
                                    <div class="price-box-offer">
                                       <ul class="offer-list">
                                       	<li><span>Full</span> Email Support</li>
                                        <li>Unlimited Backups</li>
                                        <li>All File Types Accepted</li>
                                        <li>Keep <span>30 Versions</span> of Any File</li>
                                        <li>Access Your Files From Your Mobile</li>
                                        <li>Access Your Files From Your Browser</li>
                                        <li>Restore Deleted Files For <span>30 Days</span></li>
                                       </ul>
                                    </div>
                                    <div class="sign-up-btn">
                                    	<form action="members/process" method="POST">
										  <script
										    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
										    data-key="pk_test_hr3S0Qu6OFXSP6MkQRyAMpnV"
										    data-amount="1500"
										    data-name="Sweet File"
										    data-description="Silver"
										    data-billing-address="true"
										    data-allow-remember-me="false"
										    data-image="img/sweetfilesquare.jpg">
										  </script>
										  <input type="hidden" name="plan_name" value="Silver"></input>
										  <input type="hidden" name="plan_price" value="1500"></input>
										</form>
                                    </div>
                            	</div>           
                            </div>
                                                                   
                    	</div>        
                	</div>
            	</div>
            </section>        
            <!-- End Price List -->
            
            <!-- Start Twitter Feed -->
            <section id="twitter-feed" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">  	
                    <div class="container">
                    	<div class="row text-center">
                        	<div class="col-md-12">
                              <p><i class="fa fa-twitter fa-5x highlight"></i></p>
                               <h4><a href="https://twitter.com/sweetfile" target="_blank" style="color:#121212;">@SweetFile<br><small>Our Latest Tweets</small></a></h4>
                            </div>
                            <div class="col-md-12 tweets-slider">              
                              <div id="feed"></div>  
                            </div>
                        </div>
                    </div>
            </section>
        	<!-- End Twitter Feed -->
        	
            <!-- Counter -->
            <section id="fun-facts">
            	<div class="container">
                	<div class="row wow flipInX">
                    	<div class="col-md-3 col-sm-6 fact-container">
                        	<div class="fact">
                            	<span class="counter highlight">1,200</span>
                                <p class="lead">Satisfied Customers</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 fact-container">
                        	<div class="fact">
                            	<span class="counter highlight">185</span>
                                <p class="lead">TB's of Data Stored</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 fact-container">
                        	<div class="fact">
                            	<span class="counter highlight">1.2</span>
                                <p class="lead">Avg File Size (MB)</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 fact-container">
                        	<div class="fact">
                            	<span class="counter highlight">213</span>
                                <p class="lead">High-Fives (Last 3 Months)</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Counter -->
            
            <!-- Start Get Connected -->
            <section id="get-connected" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="">
                <div class="row text-center" style="position:relative;">
                	<div class="parallax-overlay"></div> 
                	
                    <div class="container connected-row text-center wow flipInX">
                            <h2><small class="white">Get Connected <br></small>Follow Us</h2>
                            <p class="lead">We talk about anything related to computers and the online world.</p>

                    	<ul class="connected-icons">
 							<li class="col-sm-2 col-xs-offset-3 connected-icon"><a target="_blank" href="https://www.facebook.com/sweetfilebackup">
                                	<span class="fa-stack fa-lg fa-4x">
                                     	<i class="fa fa-circle fa-stack-2x"></i>
                                    	<i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                	</span><br>
                                    <span class="lead">Facebook</span><br>
                                    <span class="white">Be Our Friend</span>
                                </a>
                            </li>
                            <li class="col-sm-2 col-xs-6 connected-icon"><a target="_blank" href="http://twitter.com/sweetfile">
                                	<span class="fa-stack fa-lg fa-4x">
                                     	<i class="fa fa-circle fa-stack-2x"></i>
                                    	<i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                	</span><br>
                                    <span class="lead">Twitter</span><br>
                                    <span class="white">Follow Us</span>
                                </a>
                            </li>
                            <li class="col-sm-2 col-xs-6 connected-icon"><a target="_blank" href="#">
                                	<span class="fa-stack fa-lg fa-4x">
                                     	<i class="fa fa-circle fa-stack-2x"></i>
                                    	<i class="fa fa-google-plus-square fa-stack-1x fa-inverse"></i>
                                	</span><br>
                                    <span class="lead">Google+</span><br>
                                    <span class="white">Join Our Circle</span>
                                </a>
                            </li>
                    	</ul>        
                	</div>    
                </div>
            </section>
            <!-- End Get Connected -->

            <!-- Start Contact Form -->
            <section id="contact-form">
            	<div class="container">
                	<div class="col-lg-12 section-title-price wow flipInX">
                        <h2><small>Have a Question?</small><br><strong>Contact Us</strong></h2>
                        <p class="lead">We love <span class="highlight">hearing from you.</span></p>
                    </div>
                	<div class="col-lg-12 text-center wow flipInX" id="contact">
                    	<div id="message"></div>
                        <form method="post" action="contact.php" name="contactform" id="contactform">
                        <fieldset>
                        <div class="col-md-6"> 
                            <input name="name" type="text" id="name" size="30" value="" placeholder="Name"/>
                            <br />
                            <input name="email" type="text" id="email" size="30" value="" placeholder="Email"/>
                		</div>
                        <div class="col-md-6"> 
                            <textarea name="comments" cols="40" rows="5" id="comments" placeholder="Message"></textarea>
                        </div>
                        <div class="col-md-12 text-center">
                            <input type="submit" class="submit" id="submit" value="Submit" />
                        </div>
                        </fieldset>
                        </form>
                    </div>
                </div>
            </section>
            <!-- End Contact Form -->

            <div id="back-to-top"><a href="#"><i class="ion-arrow-up-b ion-3x"></i></a></div>    
            <!-- Start Footer -->
            <footer id="footer">
            	<div class="col-lg-12 text-center">
            		<div class="back-to-top">
                        <i class="fa fa-angle-double-up"></i>
                    </div>
                </div>
            	<div class="container text-center">
                	<div class="row">
                    	<div class="col-md-12 footer-logo">
                        	<a href="#"><h4 class="white">Sweet File</h4></a>
                        </div>
                        <div class="footer-info">
                        	<p class="footer-copyright white">Copyright © 2014 <a href="#Home">Sweet File</a> is a MediaLeaf Technologies, Inc. Company. All Rights Reserved.</p>
                        </div>
        			</div>
                </div>
            </footer>
            <!-- End Footer -->
		</div>   


