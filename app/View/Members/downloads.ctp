<!-- Start Downloads -->
<section id="services">
	<div class="container">
		<div class="col-lg-12 section-title wow flipInX">
			<h2>Software Downloads</h2>
			<p class="lead">Windows, Mac, Android, or iOS, the Sweet File software is for you.</p>
		</div>
		<div class="services-container">
			<div class="col-md-5 col-md-offset-3 col-sm-6 wow flipInX">
				<div class="service-icon">
					<i class="icon ion-social-windows ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><a href="">Download Windows Software</a></h4>
					<p class="service-description"><em>20.4 MB. Updated July 1, 2014</em></p>
				</div>                       
				<div class="service-icon">
					<i class="icon ion-social-apple ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><a href="">Download Mac Software</a></h4>
					<p class="service-description"><em>20.4 MB. Updated July 1, 2014</em></p>
				</div>
				<div class="service-icon">
					<i class="icon ion-iphone ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><a href="">Download iOS Software</a></h4>
					<p class="service-description"><em>20.4 MB. Updated July 1, 2014</em></p>
				</div>
				<div class="service-icon">
					<i class="icon ion-social-android ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><a href="">Download Android Software</a></h4>
					<p class="service-description"><em>20.4 MB. Updated July 1, 2014</em></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Downloads -->