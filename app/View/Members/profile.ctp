<?php if(isset($error)){ ?>
<div id="error_msg" style="text-align:center;color:red;font-weight:bold;margin-top:10px;">
<?php echo $error; ?>
</div>
<?php } ?>
<?php if(isset($success)){ ?>
<div id="success_msg" style="text-align:center;color:green;font-weight:bold;margin-top:10px;">
<?php echo $success; ?>
</div>
<?php } ?>
<!-- Start profile form -->
<section id="contact-form">
	<div class="container">
		<div class="col-lg-12 section-title-price wow flipInX">
			<h2><small>Change your account details</small></h2>
		</div>
		<div class="col-lg-12 text-center  flipInX" id="contact">
			<?php echo $this->Form->create('Member',array('url'=>array('controller'=>'Members','action'=>'profile'),'id'=>'profileform')); ?>
				<fieldset>
					<div class="col-md-4 col-md-push-4 text-center"> 
						<?php echo $this->Form->input('id',array('id'=>'userID','type'=>'hidden','size'=>30,'placeholder'=>'id','div'=>false,'label'=>false)); ?>
						<?php echo $this->Form->input('email',array('id'=>'email','size'=>30,'placeholder'=>'Email','div'=>false,'label'=>false)); ?>
						<br />
						<?php echo $this->Form->input('name',array('id'=>'name','size'=>30,'placeholder'=>'Name','div'=>false,'label'=>false)); ?>
						<br />
						<?php echo $this->Form->input('address1',array('id'=>'address','size'=>30,'placeholder'=>'Address','div'=>false,'label'=>false)); ?>
						<br />
						<?php echo $this->Form->input('city',array('id'=>'city','size'=>30,'placeholder'=>'City','div'=>false,'label'=>false)); ?>
						<br />
						<?php echo $this->Form->input('country',array('id'=>'country','size'=>30,'placeholder'=>'State','div'=>false,'label'=>false)); ?>
						<br />
						<?php echo $this->Form->input('postal',array('id'=>'postal','size'=>30,'placeholder'=>'Postal','div'=>false,'label'=>false)); ?>
					</div>
					<div class="col-md-12 text-center">
						<input type="submit" class="submit" id="submit" value="Update" />
					</div>
				</fieldset>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>
<!-- End profile form -->

<!-- Start password form -->
<section id="contact-form">
	<div class="container">
		<div class="col-lg-12 section-title-price wow flipInX">
			<h2><small>Change your account password</small></h2>
		</div>
		<div class="col-lg-12 text-center  flipInX" id="contact">
			<?php echo $this->Form->create('Member',array('url'=>array('controller'=>'Members','action'=>'profile'),'id'=>'passwordform')); ?>
				<fieldset>
					<div class="col-md-4 col-md-push-4 text-center"> 
						<?php echo $this->Form->input('id',array('id'=>'userID','type'=>'hidden','size'=>30,'placeholder'=>'id','div'=>false,'label'=>false)); ?>
						<?php echo $this->Form->input('oldpass',array('id'=>'oldpass','type'=>'password','size'=>30,'placeholder'=>'Current Password','div'=>false,'label'=>false)); ?>
						<br />
						<?php echo $this->Form->input('newpass',array('id'=>'newpass','type'=>'password','size'=>30,'placeholder'=>'New Password','div'=>false,'label'=>false)); ?>
						<br />
						<?php echo $this->Form->input('confpass',array('id'=>'confpass','type'=>'password','size'=>30,'placeholder'=>'New Password Again','div'=>false,'label'=>false)); ?>
					</div>
					<div class="col-md-12 text-center">
						<input type="submit" class="submit" id="submit" value="Update" />
					</div>
				</fieldset>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>
<!-- End password form -->