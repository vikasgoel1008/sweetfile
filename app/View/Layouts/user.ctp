<!DOCTYPE html>
<html lang="en">
	<head>
	
    	<!-- Meta Data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title_for_layout; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        

		
    <?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('bootstrap', 'style', 'font-awesome.min', 'ionicons.min', 'liquid-slider', 'animate', 'magnific-popup', 'YTPlayer', 'flexslider', 'blue'));
		echo $this->Html->css(array('raleway', 'montserrat'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    	       
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
	</head>       
       
       
		<?php echo $this->Session->flash(); ?>
		<body>
        <!--Start Header-->
        <section id="fixed-navbar">
			<nav class="navbar navbar-default navbar-static-top" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					    </button>
                        <!--<a class="navbar-brand" rel="home" href="/" title="Sweet File"><h3>Sweet File</h3></a>-->
                        
						<?php echo $this->Html->link($this->Html->image('sweetfile.gif',array('height'=>50)),array('controller'=>'pages','action'=>'display','home'),array('rel'=>'home','title'=>'Sweet File','escape'=>false,'class'=>'navbar-brand')); ?>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="main-nav">
                      <ul class="nav navbar-nav  navbar-right">
                        <li><?php 
                        if(($this->params['controller']=='Pages')&&($this->params['action']=='display'))
                        {
                        	echo $this->Html->link('<span class="label label-primary">Home</span>',array('controller'=>'pages','action'=>'display','home'),array('escape'=>false));
                        }
                        else
                        {
                        	echo $this->Html->link('Home',array('controller'=>'pages','action'=>'display','home'));
                        }
                        ?></li>
                        <li><?php 
                        	if(($this->params['controller']=='Members')&&($this->params['action']=='dashboard'))
                        	{
                        		echo $this->Html->link('<span class="label label-primary">Dashboard</span>',array('controller'=>'Members','action'=>'dashboard'),array('escape'=>false));
                        	}
                        	else
                        	{
                        		echo $this->Html->link('Dashboard',array('controller'=>'Members','action'=>'dashboard'),array('escape'=>false));
                        	}
                        ?></li>
                        <li><?php
						if(($this->params['controller']=='Members')&&($this->params['action']=='downloads'))
						{
                        	echo $this->Html->link('<span class="label label-primary">Downloads</span>',array('controller'=>'Members','action'=>'downloads'),array('escape'=>false));
                        }
                        else
                        {
                        	echo $this->Html->link('Downloads',array('controller'=>'Members','action'=>'downloads'));
                        }
                        ?></li>
                        <li><a href="#">Plans</a></li>
                        <li><a href="#contact-form">Contact</a></li>
                      </ul>
                    </div><!-- /.navbar-collapse -->
				</div><!-- /.container -->
			</nav>
		</section>
        <!--End Header-->
        <div class="site-wrapper">
		<?php echo $this->fetch('content'); ?>
		<div id="back-to-top"><a href="#"><i class="ion-arrow-up-b ion-3x"></i></a></div>
		<!-- Start Footer -->
		<footer id="footer">
			<div class="col-lg-12 text-center">
				<div class="back-to-top">
				<i class="fa fa-angle-double-up"></i>
				</div>
			</div>
			<div class="container text-center">
				<div class="row">
					<div class="col-md-12 footer-logo">
						<a href="#"><h4 class="white">Sweet File</h4></a>
					</div>
					<div class="footer-info">
						<p class="footer-copyright white">Copyright © 2014 <a href="#Home">Sweet File</a> is a MediaLeaf Technologies, Inc. Company. All Rights Reserved.</p>
					</div>
				</div>
			</div>
		</footer>
		<!-- End Footer -->
        </div>
       
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <?php 
        echo $this->Html->script('bootstrap.min.js');
        echo $this->Html->script('jquery.mb.YTPlayer.js');
        echo $this->Html->script('jquery.stellar.js');
        echo $this->Html->script('jquery.sticky.js');
        echo $this->Html->script('jquery.counterup.min.js');
        echo $this->Html->script('jquery.fitvids.js');
        echo $this->Html->script('modernizr.custom.js');
        echo $this->Html->script('toucheffects');
        echo $this->Html->script('wow.min');
        echo $this->Html->script('waypoints.min');
        echo $this->Html->script('jquery.easing.1.3.min');
        echo $this->Html->script('jquery.touchSwipe.min');
        echo $this->Html->script('jquery.liquid-slider');
        echo $this->Html->script('jquery.mixitup.min');
        echo $this->Html->script('jquery.magnific-popup.min');
        echo $this->Html->script('gmap3.min');
        echo $this->Html->script('jquery.flexslider-min');
        echo $this->Html->script('jquery.backstretch.min');
        echo $this->Html->script('scripts');
        ?>
         
        
    
	</body>
</html>