<!-- Start login form -->
<section id="contact-form">
	<div class="container">
		<div class="col-lg-12 section-title-price wow flipInX">
			<h2><small>Login here to manage your account</small></h2>
		</div>
		<div class="col-lg-12 text-center  flipInX" id="contact">
			<?php echo $this->Form->create('Member',array('url'=>array('controller'=>'Members','action'=>'login'),'id'=>'loginform','name'=>'contactform')); ?>
				<fieldset>
					<div class="col-md-4 col-md-push-4 text-center"> 
						<?php echo $this->Form->input('email',array('id'=>'username','size'=>30,'placeholder'=>'Email','div'=>false,'label'=>false)); ?>
						
						<br />
						<?php echo $this->Form->input('password',array('id'=>'password','type'=>'password','size'=>30,'placeholder'=>'Password','div'=>false,'label'=>false)); ?>
						
					</div>
					<div class="col-md-12 text-center">
						<?php echo $this->Form->submit('Login',array('id'=>'submit','class'=>'submit')); ?>						
					</div>
				</fieldset>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>
<!-- End login form -->
            
  
           


