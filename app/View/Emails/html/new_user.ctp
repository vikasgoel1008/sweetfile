<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" >
	<div class="templatecontainer" style="font-size: 13px; font-family: Tahoma, Arial, Verdana; color: #202020;">    	
		<div class="header" style="color: #202020;border-bottom: 3px solid #345B9E;padding-bottom: 8px;">
			<div class="h1" style="font-size: 30px; margin-top:2%;margin-right:0;margin-bottom:1%;margin-left:0;display:block; font-weight: bold; font-family: Calibri, Tahoma, Arial; ">Sweet File</div>
		</div>
		<div class="maincontent" style="margin-left: 10px; margin-top: 15px; margin-bottom: 15px;margin-right: 10px;">
			<div class="paragraph" style="margin-top: 5px; margin-bottom: 5px;">Hello <?php echo $value['Name']; ?></div>
			<div class="paragraph" style="margin-top: 15px;">Thanks for registering with SweetFile.</div>
			
			<div class="paragraph" style="margin-top: 15px;">
				We have created a Sweet File account for you, the account details can be found below:
				<div style="font-weight: bold;">Username : <?=$value['Email']?></div>
				<div style="font-weight: bold;">Password : <?=$value['Password']?></div>
			</div>
		</div>
	</div>
</body>