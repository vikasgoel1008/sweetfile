<!DOCTYPE html>
<html lang="en">
	<head>
	
    	<!-- Meta Data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title_for_layout; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        

		
    <?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('bootstrap', 'style', 'font-awesome.min', 'ionicons.min', 'liquid-slider', 'animate', 'magnific-popup', 'YTPlayer', 'flexslider', 'blue'));
		echo $this->Html->css(array('raleway', 'montserrat'));

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    	       
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
	</head>       
       
       
		<?php echo $this->Session->flash(); ?>
		
		<?php echo $this->fetch('content'); ?>
       
       
        <!-- jQuery Plugins -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>  
        <!--<script type="text/javascript" src="js/jquery.tweet.js"></script>-->
        <script src="js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
        <script src="js/jquery.stellar.js" type="text/javascript"></script>    
        <script src="js/jquery.sticky.js" type="text/javascript"></script>   
        <script src="js/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="js/jquery.fitvids.js" type="text/javascript"></script>
        <script src="js/modernizr.custom.js"></script>
        <script src="js/toucheffects.js"></script>
        <script src="js/wow.min.js" type="text/javascript"></script>
        <script src="js/waypoints.min.js" type="text/javascript"></script>
		<script src="js/jquery.easing.1.3.min.js"></script>
		<script src="js/jquery.touchSwipe.min.js"></script>
        <script src="js/jquery.liquid-slider.js"></script>
        <script src="js/jquery.mixitup.min.js" type="text/javascript"></script>
        <script src="js/jquery.magnific-popup.min.js"></script> 
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> 
        <script type="text/javascript" src="js/gmap3.min.js"></script> 
        <script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
		<script src="js/jquery.backstretch.min.js"></script> 
		<script src="js/scripts.js" type="text/javascript"></script>
	</body>
</html>