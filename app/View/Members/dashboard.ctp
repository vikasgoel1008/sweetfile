<!-- Start Dashboard -->
<section id="services">
	<div class="container">
		<div class="col-lg-12 section-title wow flipInX">
			<h2>Account Dashboard</h2>
		</div>
		<div class="services-container">
			<div class="col-md-5 col-md-offset-3 col-sm-6 wow flipInX">
				<div class="service-icon">
					<i class="icon ion-ios7-person ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><?php echo $this->Html->link('Profile',array('controller'=>'Members','action'=>'profile')) ?></h4>
					<p class="service-description"><em>Manage your contact info and password</em></p>
				</div>
				<div class="service-icon">
					<i class="icon ion-ios7-download ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><?php echo $this->Html->link('Downloads',array('controller'=>'Members','action'=>'downloads')) ?></h4>
					<p class="service-description"><em>Download our software to get started</em></p>
				</div>
				<div class="service-icon">
					<i class="icon ion-ios7-gear ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><a href="">Account</a></h4>
					<p class="service-description"><em>Change your plan or cancel your account</em></p>
				</div>
				<div class="service-icon">
					<i class="icon ion-card ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><a href="">Billing</a></h4>
					<p class="service-description"><em>Change your credit card</em></p>
				</div>                       
				<div class="service-icon">
					<i class="icon ion-drag ion-3x highlight"></i>
				</div>
				<div class="service-info">
					<h4><a href="">Invoices</a></h4>
					<p class="service-description"><em>View your invoices and payments</em></p>
				</div>                            
			</div>
		</div>
	</div>
</section>
<!-- End Dashboard -->