<?php

// SweetFile LiveDrive Integration
// 2014

class apiLivedrive{

	public $success = FALSE;
	public $code = "";
	public $message = "";
	public $codeReason = "";
	public $debug = FALSE;
	public $newUserId = "";
	public $userName = "";
	public $firstName = "";
	public $lastName = "";
	public $email = "";
	public $subDomain = "";
	public $displayType = "";
	public $displayStatus = "";
	public $backupSpaceUsed = "";
	public $backupCapacity = "";
	public $briefcaseSpaceUsed = "";
	public $briefcaseCapacity = "";
	
	
	// Set default values
	function __construct() {
		$this->apikey = "3b10fc4e-22ce-4f67-92d3-654937560ab3";
		$this->client = new SoapClient("http://www.livedrive.com/ResellersService/ResellerAPI.asmx?wsdl", array("trace"=> 1, "exceptions" => 0));
	}
	
	
	// Create a user account
	function addUser($email, $password, $subDomain, $briefcaseCapacity, $backupCapacity, $isSharing, $hasWebApps, $firstName, $lastName, $cvv, $productType){
		$result = $this->client->AddUserWithLimit(array(
					'apiKey' => $this->apikey, 
					'email' => $email, 
					'password' => $password, 
					'confirmPassword' => $password, 
					'subDomain' => $subDomain, 
					'BriefcaseCapacity' => $briefcaseCapacity, 
					'BackupCapacity' => $backupCapacity, 
					'isSharing' => $isSharing, 
					'hasWebApps' => $hasWebApps, 
					'firstName' => $firstName, 
					'lastName' => $lastName, 
					'cardVerificationValue' => $cvv, 
					'productType' => $productType));
		
		$this->success = FALSE;
		$this->codeReason = $result->AddUserWithLimitResult->Header->Code;
		if($this->codeReason == 'UserAdded'){
			$this->success = TRUE;
			$this->newUserId = $result->AddUserWithLimitResult->ID;
		}
	}
	
	
	// Get user data
	function getUser($userId){
		$result = $this->client->GetUser(array('apiKey' => $this->apikey, 'userID' => $userId));

		$this->success = FALSE;
		$this->codeReason = $result->GetUserResult->Header->Code;
		if($this->codeReason == 'UserFound'){
			$this->success = TRUE;
			$this->userName = $result->GetUserResult->UserName;
			$this->firstName = $result->GetUserResult->FirstName;
			$this->lastName = $result->GetUserResult->LastName;
			$this->email = $result->GetUserResult->Email;
			$this->subDomain = $result->GetUserResult->SubDomain;
			$this->displayType = $result->GetUserResult->DisplayType;
			$this->displayStatus = $result->GetUserResult->DisplayStatus;
			$this->backupSpaceUsed = $result->GetUserResult->BackupSpaceUsed;
			$this->backupCapacity = $result->GetUserResult->BackupCapacity;
			$this->briefcaseSpaceUsed = $result->GetUserResult->BriefcaseSpaceUsed;
			$this->briefcaseCapacity = $result->GetUserResult->BriefcaseCapacity;
		}
	}
	
	
	// Update a user
	function updateUser($userId, $email, $password, $subDomain, $isSharing, $hasWebApps, $firstName, $lastName){
		$result = $this->client->UpdateUser(array(
					'apiKey' => $this->apikey, 
					'userID' => $userId,
					'firstName' => $firstName, 
					'lastName' => $lastName, 
					'email' => $email, 
					'password' => $password, 
					'confirmPassword' => $password, 
					'subDomain' => $subDomain,  
					'isSharing' => $isSharing, 
					'hasWebApps' => $hasWebApps));
					
		$this->success = FALSE;
		$this->codeReason = $result->UpdateUserResult->Header->Code;
		if($this->codeReason == 'UserUpdated'){
			$this->success = TRUE;
		}			
	}
	
	
	
	// Close user account
	function closeUser($userId){
		$result = $this->client->CloseUser(array('apiKey' => $this->apikey, 'userID' => $userId));

		$this->success = FALSE;
		$this->codeReason = $result->CloseUserResult->Header->Code;
		if($this->codeReason == 'UserClosed'){
			$this->success = TRUE;
		}
	}
	
	
	// Suspend user account
	function suspendUser($userId){
		$result = $this->client->SuspendUser(array('apiKey' => $this->apikey, 'userID' => $userId, 'suspended' => true));

		$this->success = FALSE;
		$this->codeReason = $result->SuspendUserResult->Header->Code;
		if($this->codeReason == 'UserSuspended'){
			$this->success = TRUE;
		}
	}
	
	
	// Reinstate user account
	function reinstateUser($userId){
		$result = $this->client->SuspendUser(array('apiKey' => $this->apikey, 'userID' => $userId, 'suspended' => false));

		$this->success = FALSE;
		$this->codeReason = $result->SuspendUserResult->Header->Code;
		if($this->codeReason == 'UserReinstated'){
			$this->success = TRUE;
		}
	}

	
	

	

}


?>
