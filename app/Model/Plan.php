<?php
class Plan extends AppModel {

	var $name = 'Plan';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	var $hasMany = array( 'Member');
	
	function getPlan($plan_id) {
		$this->recursive = -1;
		
		return $this->find('first',array('conditions' => array('id' => $plan_id)));
		
			
	}
	
	function getPlans($args = null, $list = false, $recursive = -1) {
		$this->recursive = $recursive;
		
		if ($list) {
			return $this->find('list',array('conditions' => $args));
		}
		else {
			return $this->find('all',array('conditions' => $args));
		}
	}

	function getPlanId($name,$amount)
	{
		$plan_details = $this->find('first',array('conditions'=>array('name'=>$name,'price'=>$amount)));
		return $plan_details['Plan']['id'];
	}
}
?>