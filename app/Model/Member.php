<?php
class Member extends AppModel {

	public $belongsTo = 'Plan';	
	
 	public $validate = array(
        'name' => array('required'=>true),
	 	'address1' => array('required'=>true),
	 	'city' => array('required'=>true),
	 	'country' => array('required'=>true),
	 	'postal' => array('required'=>true),
 		'email' => array('required'=>true),
 		'oldpass' => array('required'=>true),
 		'newpass' => array('required'=>true),
 		'confpass' => array('required'=>true),
    );
    
    public function beforeSave($options = array()) {
        
        // Make sure that password hashes don't get re-hased everytime a member record is updated
        if(!empty($this->data['Member']['password']) && (strlen($this->data['Member']['password']) != 40)){
	        $this->data['Member']['password'] = AuthComponent::password($this->data['Member']['password']);
        }
        return true;
    }
	
}

?>
