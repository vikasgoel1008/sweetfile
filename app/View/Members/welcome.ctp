<!-- Start Services -->
<section id="services">
	<div class="container">
		<div class="col-lg-12 section-title wow flipInX">
			<h2><small>Thanks for signing up for Sweet File</small><br>We think you're going to love it!</h2>
			<p class="lead">Your account has been created and is ready to use. Just take note of your username and password below. We'll also email it to you.</p>
		</div>                    
		<h4>Account Info</h4>
		<p class="service-description">Username: <strong><?php echo $email; ?></strong></p>
		<p class="service-description">Password: <strong><?php echo $pass; ?></strong></p>                   
	</div>
</section>
<!-- End Services -->    



